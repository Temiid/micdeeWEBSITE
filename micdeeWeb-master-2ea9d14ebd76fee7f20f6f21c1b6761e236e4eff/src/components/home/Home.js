import React, { Component } from 'react';
import '../../App.css';
import Comp1 from './comp1';
import Comp2 from './comp2'
import Teste from '../reuse/Teste'
import Projects from '../reuse/Projects'
import Services from '../reuse/Services'
import Start from '../reuse/Start'
import Footer from '../reuse/Footer'
class Home extends Component {
  render() {
    return (
      <div className="Home">
      
       <Comp1 />
       <Comp2 />
       <Services />
       <Teste />                                                                                                                      
       <Footer />
      </div>
    );
  }
}

export default Home;
