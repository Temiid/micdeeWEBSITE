import React, { Component } from 'react';

import '../../App.css';
import slide1 from '../../assets/slider1.svg'
import logos from '../../assets/logo.svg'
import Header from '../reuse/Header';

class Comp1 extends Component {
    render() {
        return (
            <div className="App">

                <div className="comp1">

                   
                    <Header />
                    <div className="slide">

                        <div className='slideText'>
                            <div style={{ paddingTop: 0, color: 'rgba(255,255,255,0.9)'}}>OUR VALUE PROPOSITION</div>
                            <p>Bridging the gap between designers and clients </p>

                            <div style={{paddingTop: 15, fontSize: 13}} className='slideSelector'>
                                <div style={{paddingTop: 6, color: 'rgba(255,255,255,0.9)'}}>FROM PROJECT</div>
                                <div className="country">COUNTRY HOME</div>
                            </div>

                            <div className='indicators'>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'#9D2B4A', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

export default Comp1;
