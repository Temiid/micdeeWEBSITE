import React, { Component } from 'react';
import Start from '../reuse/Start';
import Footer from '../reuse/Footer';
import Header from '../reuse/Header';
import Projects from '../reuse/Projects';
import Banner3 from '../reuse/Banner3';

import '../../App.css';
import './contact.css'

class Contact extends Component {
  render() {
    return (
      <div className="App">
          <Header />
          <Banner3 title="Contact Us"/>
          <div className="contact-detail">
              <div className="detail">
                  <h2 style={{fontSize: 2.5 + 'em', lineHeight: 1.2+'em'}}>We'd love to hear<br/>from you</h2>
                  <h3  style={{padding: '5% 0'}}>Impressed with what you have seen so far?</h3>
                  <h3 style={{padding: '4% 0'}}>We bet you are!</h3>
                  <p style={{padding: '3% 0'}}>We love to work with amazing persons and organizations.
                      So tell us about your project, Make an enquiry, or simply 
                      asy Hi!
                  </p>
                  <hr/>
                  <p>Yabatech GRA, Ikorodu, Lagos</p>
                  <p>+234-8178869191<br/>+234-8105039737</p>
              </div>
              <div className="form">
                  <form>
                      <input style={{outline: 'none', paddingLeft: 20, fontSize: 14}} type="text" name="name" placeholder="Your name"/>
                      <input style={{outline: 'none', paddingLeft: 20, fontSize: 14}} type="text" name="email" placeholder="Email address"/>
                      <textarea style={{outline: 'none', padding: 20, fontSize: 14}} placeholder="Your Message"></textarea>
                      <button type="submit">Submit</button>
                  </form>
              </div>
          </div>
          <Projects />
           <Start />
          <Footer /> 
      </div>
    );
  }
}

export default Contact;
