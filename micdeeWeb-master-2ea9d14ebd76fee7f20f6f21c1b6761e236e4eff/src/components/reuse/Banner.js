import React, { Component } from 'react';

import '../../App.css';

class Banner extends Component {
  render() {
    return (
        <div className="banner">
              <h2>{this.props.title}</h2>
        </div>
    );
  }
}

export default Banner;
