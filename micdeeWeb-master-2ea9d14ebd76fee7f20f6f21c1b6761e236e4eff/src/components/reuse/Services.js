import React, { Component } from 'react';

import '../../App.css';
import ac from '../../assets/ac.png';
import bd from '../../assets/bd.png'
import bc from '../../assets/bc.png'
import dc from '../../assets/dc.png'
import av2 from '../../assets/av2.png'

class Services extends Component {
  render() {
    return (
      <div className="App">

        <div className="comp3">

          <div className="comp3a">

            <h3 className='header'>Services we offer</h3>

            <div className='servicesContent'>

                <div className='acImage'>
                  <div className='acImage2'>
                    <img className='' src={ac} />
                  </div>

                  <div className='acContent'>
                    <h2>Architectural Consultancy</h2>
                    <p>Our team of designers consult and produce<br />
                      designs for your projects.<br /><br />
                      Using illustrations and visuals, we are able to<br />
                      complement the design process and actualize <br />
                      your ideas from concept stage to implementation<br />
                      taking into consideration your needs and budget.
                    </p>
                  </div>
                </div>
                {/* END ONE*/}

                  <div className='acImage'>
                  <div className='acImage2'>
                    <img className='' src={dc} />
                  </div>

                  <div className='acContent'>
                    <h2>Interior Design Consultancy</h2>
                    <p>We consult and produce designs, spatial layouts, <br />
                    FF&E specifications for your renovations, re- <br />
                    models and interior projects. <br /><br />

                    Our well detailed illustrations and visuals would <br />
                    ensure your dreams come to life right before your <br />
                    very eyes.
                    </p>
                  </div>
                </div>
                {/* END ONE*/}
                
               
                
                <div style={{marginTop: 30}} className='acImage'>
                  <div className='acImage2'>
                    <img className='' src={bc} />
                  </div>

                  <div className='acContent'>
                    <h2>Brand Consultancy</h2>
                    <p>We manage buildings and projects for developers;
                    providing the best avenue for which to attract the <br />
                      right customers
                    </p>
                  </div>
                </div>
                {/* END THREE*/}
              {/* END OF servicesContentColumn */}

                <div style={{marginTop: 30}} className='acImage'>
                  <div className='acImage2'>
                    <img className='' src={av2} />
                  </div>

                  <div className='acContent'>
                    <h2>Architectural Visualisation</h2>
                    <p>We work with your creative team to create <br />
                    amazing presentations and visuals for your <br />
                    designs. <br /><br />

                    Employing still images, animations and interactive <br />
                    packages, we help put your designs in an <br />
                    atmosphere that best communicates your ideals <br />
                    and identity. <br /><br />
                    
                    We implore the use of technology and progress <br />
                    tracking tools to keep the design process <br />
                    seamless.
                  </p>
                  </div>
                </div>
                {/* END TWO*/}

                <div style={{marginTop: -190}} className='acImage'>
                  <div className='acImage2'>
                    <img className='' src={bd} />
                  </div>

                  <div className='acContent'>
                    <h2>Brand Environment Design</h2>
                    <p>We consult and create designs for brands and <br />
                      organizations that define and promote their <br />
                      brand, culture and values.
                  </p>
                  </div>
                </div>
                {/* END TWO*/}


              </div>

            </div>

          </div>

        </div>

    );
  }
}

export default Services;
