import React, { Component } from 'react';

import logos from '../../assets/logo.svg'
import pattern22 from '../../assets/pattern22.svg'
import '../../App.css';
import beh from '../../assets/approach/beh.svg';
import twi from '../../assets/approach/twi.svg';
import insta from '../../assets/approach/insta.svg';
import faceb from '../../assets/approach/faceb.svg';

class Footer extends Component {
  render() {
    return (
      <div className="App">
        <div className="comp7">
            <div className="left_content">
                <div className="logo"><img src={logos}/></div>
                <div className="contact">
                    A: Yabatech GRA, Ikorodu, Lagos<br/>
                    P: +234-8178869191 | +234-8105039737<br/>
                    E: Hello@micdee-designs.com
                </div>
                <div className="social">
                    <ul>
                        <li className="soc1"><img className='abstractImage' src={beh} alt='behance'/></li>
                        <li className="soc2"><img className='abstractImage' src={twi} alt='behance'/></li>
                        <li className="soc3"><img className='abstractImage' src={faceb} alt='behance'/></li>
                        <li className="soc4"><img className='abstractImage' src={insta} alt='behance'/></li>
                    </ul>
                </div>
            </div>
            <div className="right_content">
                <div>
                    <div>STUDIO</div>
                    <div>CAPABILITIES</div>
                    <div>WORK</div>
                    <div>CONTACT</div>
                </div>
                <div>
                    2017, Micdee Designs, All rights reserved
                </div>
            </div>
        </div>
      </div>
    );
  }
}

export default Footer;
