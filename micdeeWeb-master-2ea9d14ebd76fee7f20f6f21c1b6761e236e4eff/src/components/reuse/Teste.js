import React, { Component } from 'react';
import styled from 'styled-components';

import '../../App.css';
import comment from '../../assets/comment.svg'
import testemonial from '../../assets/testemonial.svg'
import leftArrow from '../../assets/approach/left-arrow.svg';
import rightArrow from '../../assets/approach/right-arrow.svg';

class Teste extends Component {
  render() {
    return (
      <div className="App">
        <div className="comp4">
            <div className="about">
              <div className="face_img"></div>
              <div className="left_arrow">
                <img style={{marginLeft: 50}} width={20} src={leftArrow} />
              </div>

              <div className="content">
                <img src={comment}/>
                <p>
                  I enjoyed working with the Micdee team. The rocess was very
                  collaborative, i enjoyed the design thinking exercise in determining the space 
                  my office needed
                </p>
                <hr/>
                <p>
                  ROY ADENUGA<br/>
                  GENERAL MANAGER, CORPORATE AFFAIRS<br/>
                  ADENUGA
                </p>
                <img src={testemonial}/>
              </div>

              <div className="right_arrow">
                <img style={{marginRight: 50}} width={20} src={rightArrow} />
              </div>
            </div>        
        </div>
      </div>
    );
  }
}

export default Teste;
