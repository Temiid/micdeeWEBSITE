import React, { Component } from 'react';

import '../../App.css';
import slide1 from '../../assets/slider1.svg'
import logos from '../../assets/logo.svg'
import {Link} from 'react-router-dom';

class Header extends Component {
    constructor(props){
        super(props);
        this.state = {
            menu: false
        };
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
    handleClose(event){
        console.log(event)
        this.setState({ menu: false})
    }
    handleOpen(event){
        console.log(event)
        this.setState({ menu: true})
        // const { target } = event;
        // const { value, name } = target;

        // let err = this.state.error;
        // err[name] = this.handleError(target.name, target.value);

       
        // this.setState({ 
        //     [name]: value,
        //     error: err
        // });
    }
    render() {
        return (
            <div className="comp1a">
                        <div className="nav">
                            <div className="logo">
                                <img src={logos} alt='Micdee-logo' />
                            </div>
                            <div class="hamburger" onClick={this.handleOpen}></div>

                            <ul className={`menu ${this.state.menu ? 'open' : 'close'}`}>
                                <div className="close" onClick={this.handleClose}></div>
                            <li class="nav-links">
                                <Link to={`/home`}>
                                    <a className="nav-active" href="#">HOME</a>
                                </Link>
                            </li>
                            <li class="nav-links">
                                <Link to={`/studio`}>
                                    <a href="#">STUDIO</a>
                                </Link>
                            </li>
                            <li class="nav-links">
                                <Link to={`/capabilities`}>
                                    <a href="#">CAPABILITIES</a>
                                </Link>
                            </li>
                            <li class="nav-links">
                                <Link to={`/work`}>
                                    <a href="#">WORK</a>
                                </Link>
                            </li>
                            <li class="nav-links">
                                <Link to={`/contact`}>
                                    <a href="#">CONTACT</a>
                                </Link>
                            </li>
                            
                            </ul>

                        </div>
            </div>
        );
    }
}

export default Header;
