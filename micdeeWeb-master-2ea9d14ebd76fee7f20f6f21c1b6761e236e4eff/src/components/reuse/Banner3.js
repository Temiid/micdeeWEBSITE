import React, { Component } from 'react';

import '../../App.css';

class Banner3 extends Component {
  render() {
    return (
        <div className="banner3">
              <h2>{this.props.title}</h2>
        </div>
    );
  }
}

export default Banner3;
