import React, { Component } from 'react';

import '../../App.css';

class Banner2 extends Component {
  render() {
    return (
        <div className="banner2">
              <h2>{this.props.title}</h2>
        </div>
    );
  }
}

export default Banner2;
