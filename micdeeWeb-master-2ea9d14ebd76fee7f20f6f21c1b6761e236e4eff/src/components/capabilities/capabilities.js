import React, { Component } from 'react';
import '../../App.css';
import Teste from '../reuse/Teste'
import Header from '../reuse/Header'
import Projects from '../reuse/Projects'
import Services from '../reuse/Services'
import Start from '../reuse/Start'
import Footer from '../reuse/Footer'
import Banner2 from '../reuse/Banner2'

import './capabilities.css';

class Capabilities extends Component {
  render() {
    return (
        <div className="App">
       <Header />
        <Banner2 title="Capabilities"/>

        <div className="capaa">
            <div className="capa">
                <h2>About our Services</h2>
                <p>
                    We bridge the gap between Designers and Clients by 
                    creating strategies that solve design problems to help them
                    realize their goods and connect to the right audience
                </p><br/>
                <hr/>
            </div>
            </div>
            
       <Services />
       <Teste />                                                                                                                      
       <Footer />
        </div>
    );
  }
}

export default Capabilities;
