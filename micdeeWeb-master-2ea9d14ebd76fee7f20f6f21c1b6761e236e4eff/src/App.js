import React, { Component } from 'react';
import './App.css';
import Home from './components/home/Home';
import Contact from './components/contact/Contact';
import Studio from './components/studio/studio';
import Work from './components/work/work';
import Works from './components/works/works';
import Capabilities from './components/capabilities/capabilities';

import { Route, BrowserRouter } from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <BrowserRouter> 
        <div className="App">
      {/* <Contact /> */}
      <Route path="/home" component={Home}/>
      <Route path="/contact" component={Contact}/>
      <Route path="/studio" component={Studio}/>
      <Route path="/work" component={Work}/>
      <Route path="/works" component={Works}/>
      <Route path="/capabilities" component={Capabilities}/>
        {/* {<Studio />} */}
        {/* <Work /> */}
          {/* <Work/>/>  */}
        </div>
      </BrowserRouter>
    );
  }
}

export default App;